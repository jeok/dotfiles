# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export HISTSIZE=1500
alias ..='cd ..'
alias wifi="sudo wpa_supplicant -B -i wlp12s0 -c /etc/wpa_supplicant/wpa_supplicant-wlp12s0.conf && echo 'Wait 5 sec and set dhcpcd' && sleep 5 && dhcpcd wlp12s0"
alias ls="exa"

# Misc.
PS1="\[\033[0;31m\]\u\[\033[00m\]@\[\033[0;32m\]\h:\[\033[0;36m\]\w\[\033[00m\] \$ "
alias datenow='date +%d-%m-%Y-%H:%M'
alias scrot='grim /home/janne/pics/scrots/$(datenow).png'
alias pip_upgrade="pip list --user | tail -n +3 | awk '{print }' | xargs -n1 pip install -U"
