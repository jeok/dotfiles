" Vim configs

call plug#begin('~/.local/share/nvim/plugged')

Plug 'junegunn/vim-easy-align'
Plug 'junegunn/seoul256.vim'
Plug 'danilo-augusto/vim-afterglow'

" Completion thingies and stuff
Plug 'ncm2/ncm2' | Plug 'roxma/nvim-yarp'
Plug 'ncm2/ncm2-syntax' | Plug 'Shougo/neco-syntax'

" Since I don't know what I'm doing the following is from ncm2 github page
" enable ncm2 for all buffers
autocmd BufEnter * call ncm2#enable_for_buffer()

" IMPORTANT: :help Ncm2PopupOpen for more information
set completeopt=noinsert,menuone,noselect

Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-path'
Plug 'ncm2/ncm2-jedi'

Plug 'ncm2/ncm2-ultisnips' | Plug 'SirVer/ultisnips'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-sleuth'

call plug#end()

" General
set undofile
set undodir=$HOME/.config/nvim/undo

" Settings for ncm2
" suppress the annoying 'match x of y', 'The only match' and 'Pattern not
" found' messages
set shortmess+=c

" CTRL-C doesn't trigger the InsertLeave autocmd . map to <ESC> instead.
inoremap <c-c> <ESC>

" When the <Enter> key is pressed while the popup menu is visible, it only
" hides the menu. Use this mapping to close the menu and also start a new
" line.
inoremap <expr> <CR> (pumvisible() ? "\<c-y>\<cr>" : "\<CR>")

" Use <TAB> to select the popup menu:
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Themes
" Seoul256
let g:seoul256_background = 233
colo seoul256

" Danilo Autisto's Afterglow
"let g:afterglow_blackout=1
"let g:afterglow_italic_comments=1
"let g:afterglow_inherit_background=1

"colo afterglow

