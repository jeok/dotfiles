#/bin/sh
PERCENTAGE=$(amixer sget 'Master' -M | tail -n +5 | awk '{print $4}' | tr -d []\%)
TEXT="Master"
TOOLTIP="$TEXT volume at $PERCENTAGE"
CLASS="custom-alsa"
# Waybar expects a JSON format
echo "{\"text\": \"$TEXT\", \"tooltip\": \"$TOOLTIP\", \"class\": \"$CLASS\", \"percentage\": $PERCENTAGE }"
